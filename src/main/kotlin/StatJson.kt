import com.fasterxml.jackson.annotation.JsonProperty

data class StatJson(
    val evaluation: String,
    val dimension: Int,
    val run: Int,
    val alpha: Double,
    val pc: Double,
    val pm: Double,
    val distance: String,
    val sigma: Double,
    val nfe: Int,
    @get:JsonProperty("nSeeds") val nSeeds: Int? = null,
    val np: Int? = null,
    val gp: Int? = null,
    val lp: Int? = null,
    val pr: Double? = null,
    val gpr: Double? = null,
    val lpr: Double? = null,
    val fpr: Double? = null,
    val k: Double? = null,
//    val lfp: Double,
//    val hfp: Double,
    val selection: String = "sus",
    val crossover: String = "one-point",
    val mutation: String = "density"
)