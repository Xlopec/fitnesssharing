import com.fasterxml.jackson.databind.ObjectMapper
import java.io.File

fun main(vararg args: String) {
    require(args.size <= 3) { "Invalid args size, was ${args.toList()}" }
    val dir = File(args[0]).also { require(it.isDirectory) }
    val field = args[1]
    val shouldFindMax = args.getOrNull(2) != "min"

    println("Will search for the best param value of '$field' in the directory $dir, max=$shouldFindMax")

    println(selectBestParams(dir, field, shouldFindMax))
}

fun selectBestParams(dir: File, field: String, shouldFindMax: Boolean): Pair<Double, AnalysisParams> {
    require(dir.isDirectory) { "$dir is not a directory or it doesn't exist" }

    val mapper = ObjectMapper()

    val vals = dir.listFiles { file -> file.nameWithoutExtension.startsWith("stat") && file.extension == "json" }
        .asSequence()
        .map { mapper.readTree(it) }
        .map { tree -> tree to tree.path(field).asDouble() }

    val (tree, v) = if (shouldFindMax) vals.maxBy { (_, v) -> v }!! else vals.minBy { (_, v) -> v }!!

    return v to AnalysisParams(
        alpha = tree.path("alpha").asDouble(.0),
        crossOverProbability = tree.path("pc").asDouble(),
        mutationProbability = tree.path("pm").asDouble()
    )
}

data class AnalysisParams(val alpha: Double, val crossOverProbability: Double, val mutationProbability: Double)
