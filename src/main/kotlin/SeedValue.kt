data class SeedValue(
    val encodedParams: Iterable<String>,
    val decodedParams: DoubleArray,
    val stringView: String,
    val health: Double
)
