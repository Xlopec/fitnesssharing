import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import java.io.File
import java.time.LocalDateTime
import kotlin.math.max
import kotlin.math.roundToInt
import kotlin.random.Random

fun RunStatistics.toJson(
    fitnessFunction: FitnessFunction,
    dimension: Int,
    epoch: Int,
    params: RunParams
): StatJson = when (this) {
    is RunStatistics.Failure -> StatJson(
        fitnessFunction.toString(),
        dimension,
        epoch,
        params.alpha,
        params.crossOverProbability,
        params.mutationProbability,
        params.distanceFunction.toString(),
        params.sigmaShare,
        nfe
    )

    is RunStatistics.Success -> StatJson(
        fitnessFunction.toString(),
        dimension,
        epoch,
        params.alpha,
        params.crossOverProbability,
        params.mutationProbability,
        params.distanceFunction.toString(),
        params.sigmaShare,
        nfe,
        nSeeds.size,
        gPeaks + lPeaks,
        gPeaks,
        lPeaks,
        pR,
        gPr,
        lPr,
        fpr,
        k
    )
}

fun main(args: Array<String>) {

    val outDir = File("out/stats").also { it.deleteRecursively(); it.mkdirs() }

    val mapper = jacksonObjectMapper()

    val jobs = mutableListOf<Deferred<*>>()

    val fitnessFunctions = listOf(DebFunction1, DebFunction2, DebFunction3, DebFunction4)

    val epochs = 5
    var doneCounter = 0

    println("Starting at ${LocalDateTime.now()}. Grab some beer \uD83C\uDF7A and watch \uD83D\uDC40")

    val testParams = testParams(args.last().toBoolean())

    for (n in args.mapNotNull { it.toIntOrNull() }) {

        val N = 500 * if (n > 3) 10 else 1

        for (runParams in testParams) {

            val runner = Runner(runParams)

            for (epoch in 0 until epochs) {

                for (fitnessFunction in fitnessFunctions) {

                    val initialPopulation = List(N) { Ind(fitnessFunction.minX.repeatTo(n).zip(fitnessFunction.maxX.repeatTo(n)).map{ Random.nextDouble(it.first, it.second) }.toDoubleArray(), fitnessFunction.minX, fitnessFunction.maxX) }

                    // todo: generate multiple solvers for different set of params but with same initial population
                    // todo: consider how to reuse code for test functions from test set S

                    jobs += GlobalScope.async {
                        //println("Running: $epoch-th run, param=$runParams, N=$N, n=$n, function=$fitnessFunction")

                        val stats = runner.run(initialPopulation, fitnessFunction, n)

                        doneCounter += 1// 'well, shit' is also result

                        print(
                            calculateProgress(
                                doneCounter / (epochs * args.size * fitnessFunctions.size * testParams.size).toDouble() * 100.0,
                                epoch, N, n, fitnessFunction, runParams
                            )
                        )

                        val nameEnding = "${fitnessFunction}_dim${n}_run$epoch" +
                                with(runner.runParams) {
                                    "_alpha${alpha}_pc${crossOverProbability}_pm$mutationProbability" +
                                    "_${distanceFunction}_sigma$sigmaShare.json"
                                }

                        mapper.writeValue(
                            File(outDir, "stat_$nameEnding"), stats.toJson(
                                fitnessFunction, n, epoch, runner.runParams
                            )
                        )
                        if (stats is RunStatistics.Success && n == 1) {
                            mapper.writeValue(
                                File(outDir, nameEnding),
                                seedsJson(
                                    listOf(stats.nSeeds) /*FIXME workaround*/,
                                    fitnessFunction
                                )
                            )

                            //println("Done: $epoch-th run, param=$runParams, N=$N, n=$n, function=$fitnessFunction, k=${stats.k}")
                        } else if (stats is RunStatistics.Failure) {
                            println("well, shit")
                        }
                    }

                }
            }

        }
    }

    runBlocking { jobs.forEach { it.await() } }

    println("Done, drink some beer \uD83C\uDF7A and relax")
}

private fun seedsJson(seeds: Iterable<Iterable<Ind>>, fitnessFunction: (DoubleArray) -> Double) = seeds.map {
    it.map { ind ->
        SeedValue(
            ind.genotype.toString().chunked(10), // TODO un-hardcode
            ind.phenotype,
            ind.genotype.toString(),
            fitnessFunction(ind.phenotype)
        )
    }
}

private fun testParams(shouldSampleDuration: Boolean): Collection<RunParams> {

    val params = mutableListOf<RunParams>()

    val distanceToSigmas = listOf(
        EuclideanDistance to .1, EuclideanDistance to .2, EuclideanDistance to .4,
        HammingDistance to 4.0, HammingDistance to 2.0, HammingDistance to 6.0
    )

    for (alpha in listOf(1.0, 2.0, .5)) {

        for (crossoverProbability in listOf(1.0, .8, .6)) {

            for (mutationsProbability in listOf(.01, .001)) {

                for ((distanceFun, sigma) in distanceToSigmas) {

                    params += RunParams(
                        algorithm = ::geneticAlgorithm,
                        alpha = alpha,
                        crossOverProbability = crossoverProbability,
                        mutationProbability = mutationsProbability,
                        distanceFunction = distanceFun,
                        sigmaShare = sigma,
                        shouldSampleDuration = shouldSampleDuration
                    )
                }
            }
        }
    }

    return params
}

private fun calculateProgress(
    percentage: Double, epoch: Int, N: Int, n: Int,
    fitnessFunction: FitnessFunction,
    runParams: RunParams, timestamp: LocalDateTime = LocalDateTime.now(), bricks: UInt = 100U
): String {

    val cBricks = ((percentage.roundToInt() / 100.0) * bricks.toInt()).roundToInt()

    return StringBuilder()
        .append('\r')
        .append('Є')
        .append(*Array(cBricks) { "=" })
        .append(if (cBricks == bricks.toInt()) "" else "Э")
        .append(*Array(max(bricks.toInt() - cBricks - 1, 0)) { " " })//clear
        .append("(_*_)")
        .append((" %.2f/100%%, %s. Done %d-th epoch, N=%d, n=%d, " +
                    "function=%s, params=%s").format(percentage, timestamp, epoch, N, n, fitnessFunction, runParams))
        .toString()
}
