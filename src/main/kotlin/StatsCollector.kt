import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import java.io.File
import java.time.LocalDateTime
import kotlin.math.max
import kotlin.math.roundToInt
import kotlin.random.Random

class StatsCollector(
    val outDir: File, val dimension: Int, val testParams: Collection<RunParams>,
    val fitnessFunctions: Collection<FitnessFunction>, val epochs: Int = 10
) {

    init {
        outDir.deleteRecursively()
        outDir.mkdirs()
    }

    fun collect() {
        val mapper = jacksonObjectMapper()
        val jobs = mutableListOf<Deferred<*>>()
        var doneCounter = 0

        val N = 500 * if (dimension > 3) 10 else 1

        for (runParams in testParams) {

            val runner = Runner(runParams)

            for (epoch in 0 until epochs) {


                for (fitnessFunction in fitnessFunctions) {

                    val initialPopulation = List(N) { Ind(fitnessFunction.minX.repeatTo(dimension).zip(fitnessFunction.maxX.repeatTo(dimension)).map{ Random.nextDouble(it.first, it.second) }.toDoubleArray(), fitnessFunction.minX, fitnessFunction.maxX) }

                    jobs += GlobalScope.async {
                        //println("Running: $epoch-th run, param=$runParams, N=$N, n=$n, function=$fitnessFunction")

                        val stats = runner.run(initialPopulation, fitnessFunction, dimension)

                        doneCounter += 1// 'well, shit' is also result

                        print(
                            calculateProgress(
                                doneCounter / (epochs * fitnessFunctions.size * testParams.size).toDouble() * 100.0,
                                epoch, N, dimension, fitnessFunction, runParams
                            )
                        )

                        val nameEnding = "${fitnessFunction}_dim${dimension}_run$epoch" +
                                with(runner.runParams) {
                                    "_alpha${alpha}_pc${crossOverProbability}_pm$mutationProbability" +
                                            "_${distanceFunction}_sigma$sigmaShare.json"
                                }

                        mapper.writeValue(
                            File(outDir, "stat_$nameEnding"), stats.toJson(
                                fitnessFunction, dimension, epoch, runner.runParams
                            )
                        )
                        if (stats is RunStatistics.Success && dimension == 1) {
                            mapper.writeValue(
                                File(outDir, nameEnding),
                                seedsJson(
                                    listOf(stats.nSeeds) /*FIXME workaround*/,
                                    fitnessFunction
                                )
                            )

                            //println("Done: $epoch-th run, param=$runParams, N=$N, n=$n, function=$fitnessFunction, k=${stats.k}")
                        } else if (stats is RunStatistics.Failure) {
                            println("well, shit")
                        }
                    }

                }
            }
        }

        runBlocking { jobs.forEach { it.await() } }
    }

}

private fun seedsJson(seeds: Iterable<Iterable<Ind>>, fitnessFunction: (DoubleArray) -> Double) = seeds.map {
    it.map { ind ->
        SeedValue(
            ind.genotype.toString().chunked(10), // TODO un-hardcode
            ind.phenotype,
            ind.genotype.toString(),
            fitnessFunction(ind.phenotype)
        )
    }
}

private fun calculateProgress(
    percentage: Double, epoch: Int, N: Int, n: Int,
    fitnessFunction: FitnessFunction,
    runParams: RunParams, timestamp: LocalDateTime = LocalDateTime.now(), bricks: UInt = 100U
): String {

    val cBricks = ((percentage.roundToInt() / 100.0) * bricks.toInt()).roundToInt()

    return StringBuilder()
        .append('\r')
        .append('Є')
        .append(*Array(cBricks) { "=" })
        .append(if (cBricks == bricks.toInt()) "" else "Э")
        .append(*Array(max(bricks.toInt() - cBricks - 1, 0)) { " " })//clear
        .append("(_*_)")
        .append(
            (" %.2f/100%%, %s. Done %d-th epoch, N=%d, n=%d, " +
                    "function=%s, params=%s").format(percentage, timestamp, epoch, N, n, fitnessFunction, runParams)
        )
        .toString()
}
