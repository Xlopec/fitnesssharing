import kotlin.math.max
import kotlin.math.pow
import kotlin.math.sqrt

object EuclideanDistance : (Ind, Ind) -> Double {

    override fun invoke(from: Ind, to: Ind): Double {
        require(from.phenotype.size == to.phenotype.size) {
            "Phenotypes must have same size, from=${from.phenotype.size}, to=${to.phenotype.size}"
        }
        return sqrt(from.phenotype.indices.sumByDouble { i -> (from.phenotype[i] - to.phenotype[i]).let { it * it } })
    }

    override fun toString(): String = "euclidian"

}

object HammingDistance : (Ind, Ind) -> Double {

    override fun invoke(from: Ind, to: Ind): Double {
        return (0 until max(from.genotype.length, to.genotype.length)).sumBy { i -> from.genotype[i] - to.genotype[i] }.toDouble()
    }

    override fun toString(): String = "hamming"

}

fun sharingFunction(nicheRadius: Double, alpha: Double, distance: Double): Double {
    return if (distance < nicheRadius) (1.0 - (distance / nicheRadius).pow(alpha)) else .0
}

fun Ind.sharedFitnessFunction(other: Iterable<Ind>, fitnessFunction: (DoubleArray) -> Double, runParams: RunParams): Double {

    val m = other.sumByDouble { ith ->
        sharingFunction(
            runParams.sigmaShare,
            runParams.alpha,
            runParams.distanceFunction(this, ith)
        )
    }

    return if (m == 0.0) fitnessFunction(this.phenotype) else fitnessFunction(this.phenotype) / m
}

private operator fun Boolean.minus(another: Boolean): Int {
    return if (this == another) 0 else 1
}