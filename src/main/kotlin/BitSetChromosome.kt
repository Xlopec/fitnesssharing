import java.util.*

class BitSetChromosome(private val bitSet: BitSet, val length: Int) {
    // calculate once
    private val string by lazy { (0 until length).map { if (get(it)) '1' else '0' }.joinToString("") }

    constructor(length: Int): this(BitSet(length), length)

    fun set(i: Int) = bitSet.set(i)

    operator fun set(i: Int, value: Boolean) = if (value) set(i) else Unit

    operator fun get(i: Int) = bitSet[i]

    override fun toString() = string
//        val words = bitSet.toLongArray()
//        val parts = (if (words.isEmpty()) longArrayOf(0) else words).map { toBinaryString(it) }
//        val head = String.format("%${length % WORD_LENGTH}s", parts.last())
//        val tail = parts.dropLast(1).joinToString("") { String.format("%${WORD_LENGTH}s", it) }
//        return (tail + head).replace(' ', '0')
//    }
}

private const val WORD_LENGTH = 64