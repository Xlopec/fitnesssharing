import java.lang.Integer.parseInt
import java.util.*
import kotlin.math.log2
import kotlin.math.min
import kotlin.math.roundToInt

typealias Ind = Individual<DoubleArray, BitSetChromosome>
//private const val MIN_VALUE = 0.0
//private const val MAX_VALUE = 1.0
private const val ENCODING_PRECISION = 0.001
//private val

fun Ind(genotype: DoubleArray, min: DoubleArray, max: DoubleArray): Ind = Individual(genotype, genotype.phenotype(min, max), min, max)

fun Ind(phenotype: BitSetChromosome, min: DoubleArray, max: DoubleArray): Ind = Individual(phenotype.genotype(min, max), phenotype, min, max)

private fun DoubleArray.phenotype(mins: DoubleArray, maxes: DoubleArray): BitSetChromosome {
    val allMins = mins.repeatTo(size)
    val allMaxes = maxes.repeatTo(size)
    val geneLengthes = allMins.zip(allMaxes).map { Math.ceil(log2((it.second - it.first)/ENCODING_PRECISION)).toInt() }
    val res = BitSetChromosome(geneLengthes.sum())

    var accumulator = 0
    forEachIndexed { i, x ->
        val binary = ((x - allMins[i]) / ENCODING_PRECISION).roundToInt()
        val gray = binary xor (binary shr 1)
        val filterRes = (0..geneLengthes[i]).filter { (gray and (1 shl it)) > 0 }
        val mapRes = filterRes.map { geneLengthes[i] - it - 1 }
        mapRes.forEach {
            if (it < 0) {
                println(this)
            }
            res.set(accumulator + it)
        }
        accumulator += geneLengthes[i]
    }

    return res
}

private fun BitSetChromosome.genotype(mins: DoubleArray, maxes: DoubleArray): DoubleArray {
    val repeatingLength = mins.zip(maxes).map { Math.ceil(log2((it.second - it.first)/ENCODING_PRECISION)).toInt() }.toIntArray()
    val size = repeatingLength.size * (length / repeatingLength.sum()) + amountInLast(repeatingLength, length % repeatingLength.sum())
    val geneLengthes = repeatingLength.repeatTo(size)
    val allMaxes = maxes.repeatTo(size)

    val grays = toString().chunked(geneLengthes).map { parseInt(it, 2) } // fixme
    return grays . mapIndexed { i, it ->
        var num = it
        var mask = num shr 1
        while (mask != 0) {
            num = num xor mask
            mask = mask shr 1
        }
        min(num * ENCODING_PRECISION, allMaxes[i])
    } . toDoubleArray()
}

public fun fromStringJustForTest(genotype: String): Ind {
    val bitSet = BitSet(genotype.length)
    (0 until genotype.length).filter { genotype[it] == '1' }.forEach { bitSet.set(it) }
    return Ind(BitSetChromosome(bitSet, genotype.length), doubleArrayOf(0.0), doubleArrayOf(1.0))
}

public fun DoubleArray.repeatTo(newSize: Int) = (0 until newSize).fold(DoubleArray(newSize)) { arr, i -> arr[i] = this[i % this.size]; arr }
public fun IntArray.repeatTo(newSize: Int) = (0 until newSize).fold(IntArray(newSize)) { arr, i -> arr[i] = this[i % this.size]; arr }

private fun String.chunked(ns: IntArray): List<String> {
    require(ns.sum() == length)

    val result = mutableListOf<String>()

    var to = 0
    for (n in ns) {

        val from = to
        to += n

        result += substring(from, to)
    }

    return result
}

private fun amountInLast(lengthes: IntArray, part: Int): Int {
    var sum = 0
    var i = 0
    while (part > sum) {
        sum += lengthes[i++]
    }
    require(part == sum)

    return i
}
