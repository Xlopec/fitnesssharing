import kotlin.math.*

/*
Required functions are the following:
* F46 (спини шестигорбого верблюда)
* F20 (Растригіна)
* F31 (Хін-Ши Янга 2)
* F22 (Гріванга)
* D6
* F20-m2
 */

object SixHumpCamelBack : FitnessFunction {

    private val globalPeaksX1 = doubleArrayOf(-.0898, .0898)
    private val globalPeaksX2 = doubleArrayOf(-.7126, .7126)
    private val allPeaksX1 = doubleArrayOf(*globalPeaksX1, -1.7036, 1.7036, -1.6071, 1.6071)
    private val allPeaksX2 = doubleArrayOf(*globalPeaksX2, -.796, .7961, -.5687, .5687)

    override val minX = doubleArrayOf(-3.0, -2.0)
    override val maxX = doubleArrayOf(3.0, 2.0)

    override val minValue: Double = -1.0316

    override fun invoke(individual: DoubleArray): Double {
        require(individual.size == 2)

        val x1 = individual[0]
        val x2 = individual[1]

        return -((4.0 - 2.1 * x1.pow(2) + x1.pow(4) / 3) * x1.pow(2) + x1 * x2 + 4.0 * (x2.pow(2) - 1) * x2.pow(2))
    }

    override fun closestPeak(individual: Ind): Extremum {
        require(individual.phenotype.size == 2)

        val x1 = closestPeak(allPeaksX1, individual.phenotype[0])
        val x2 = closestPeak(allPeaksX2, individual.phenotype[1])

        return Extremum(Ind(doubleArrayOf(x1, x2), minX, maxX), globalPeaksX1.contains(x1) && globalPeaksX2.contains(x2))
    }

    override fun peaks(n: Int): Int {
        require(n == 2)
        return allPeaksX1.size
    }

    override fun globalPeaks(n: Int): Int {
        require(n == 2)
        return 2
    }

    override fun toString(): String = "Six_hump_camel_back"
}

object Rastrigin : FitnessFunction {

    private const val globalPeak = .0
    private val allPeaks = doubleArrayOf(*(-5..5).map { it.toDouble() }.toDoubleArray())

    override val minX = doubleArrayOf(-5.12)
    override val maxX = doubleArrayOf(5.12)

    override val minValue: Double = .0

    override fun invoke(individual: DoubleArray): Double {
        return individual.sumByDouble(::f) - 10 * individual.size
    }

    override fun closestPeak(individual: Ind): Extremum {
        return closestPeakIndividual(allPeaks, individual).let { peak ->
            Extremum(peak, peak.phenotype.all { p -> p == globalPeak })
        }
    }

    override fun peaks(n: Int): Int = allPeaks.size.toDouble().pow(n).toInt()

    override fun globalPeaks(n: Int): Int = 1

    private fun f(x_i: Double) = 10.0 * cos(2 * PI * x_i) - x_i.pow(2)

    override fun toString(): String = "Rastrigin"
}

/**
Although, there is one unique global minima at the origin (i.e., fmin(X∗)=0 @ x∗i=0),
there are infinite number of almost global minimum with so small errors (maybe less than 10−86).
Thus, this function is not recommended to be used for minimization
[see](https://al-roomi.org/benchmarks/unconstrained/n-dimensions/264-xin-she-yang-s-function-no-02)

This implementation supports only n=2.

It's not clear how to find local extrems
 */
@Deprecated("oh shi")
object XinSheYang : FitnessFunction {
    private val globalPeaks = doubleArrayOf(-.5, .5)

    override val maxX = doubleArrayOf(-10.0)
    override val minX = doubleArrayOf(10.0)

    override val minValue: Double = TODO()

    override fun invoke(individual: DoubleArray): Double {
        return individual.sumByDouble(::abs) * exp(-individual.sumByDouble { it.pow(2) })
    }

    override fun closestPeak(individual: Ind): Extremum {
        require(individual.phenotype.size == 2)

        val x1 = closestPeak(globalPeaks, individual.phenotype[0])
        val x2 = closestPeak(globalPeaks, individual.phenotype[1])

        return Extremum(Ind(doubleArrayOf(x1, x2), minX, maxX), globalPeaks.contains(x1) && globalPeaks.contains(x2))
    }

    override fun peaks(n: Int): Int {
        require(n == 2)
        TODO("implement")
    }

    override fun globalPeaks(n: Int): Int {
        require(n == 2)

        return 4
    }

    override fun toString(): String = "Xin-She-Yang"
}

@Deprecated("oh shi")
object Griewangk : FitnessFunction {

    private const val globalPeak = .0
    private val allPeaks = doubleArrayOf(globalPeak, *(6.28005..6.28005 * 81 step 6.28005).toList().toDoubleArray())

    override val maxX = doubleArrayOf(-600.0)
    override val minX = doubleArrayOf(600.0)

    override val minValue: Double = TODO()

    override fun invoke(individual: DoubleArray): Double {

        val sum = individual.sumByDouble { x_i -> x_i.pow(2) / 4000 }

        val product = individual.mapIndexed { i, x_i -> cos(x_i / sqrt(i + 1.0)) }.fold(1.0) { acc, d -> acc * d }

        return sum - product + 1
    }

    override fun closestPeak(individual: Ind): Extremum {
        return closestPeakIndividual(allPeaks, individual).let { peak ->
            Extremum(peak, peak.phenotype.all { p -> p == globalPeak })
        }
    }

    override fun peaks(n: Int): Int = allPeaks.size

    override fun globalPeaks(n: Int): Int = 1

    override fun toString(): String = "Griewangk"

}

object FiveUnevenPeakTrap : FitnessFunction {

    override val minX = doubleArrayOf(.0)
    override val maxX = doubleArrayOf(30.0)

    private val globalPeaks = doubleArrayOf(.0, 30.0)
    private val allPeaks = doubleArrayOf(*globalPeaks, 5.0, 12.5, 22.5)

    override val minValue: Double = .0

    override fun invoke(individual: DoubleArray): Double {
        require(individual.size == 1)

        val x = individual[0]

        return when {
            x >= .0 && x < 2.5 -> 80.0 * (2.5 - x)
            x >= 2.5 && x < 5.0 -> 64.0 * (x - 2.5)
            x >= 5.0 && x < 7.5 -> 64.0 * (7.5 - x)
            x >= 7.5 && x < 12.5 -> 28.0 * (x - 7.5)
            x >= 12.5 && x < 17.5 -> 28.0 * (17.5 - x)
            x >= 17.5 && x < 22.5 -> 32.0 * (x - 17.5)
            x >= 22.5 && x < 27.5 -> 32.0 * (27.5 - x)
            x in 27.5..30.0 -> 80.0 * (x - 27.5)
            else -> throw IllegalArgumentException("What a Terrible Failure, individual=${individual[0]}")
        }
    }

    override fun closestPeak(individual: Ind): Extremum {
        require(individual.phenotype.size == 1)

        return closestPeakIndividual(allPeaks, individual).let { peak ->
            Extremum(peak, peak.phenotype.all(globalPeaks::contains))
        }
    }

    override fun peaks(n: Int): Int {
        require(n == 1)
        return allPeaks.size
    }

    override fun globalPeaks(n: Int): Int {
        require(n == 1)
        return globalPeaks.size
    }

    override fun toString(): String = "Five-Uneven-Peak-Trap"
}

/**
 * Implemented only for n=2
 */
@Deprecated("oh shi")
object RastriginModified2 : FitnessFunction {

    private val globalPeaks = doubleArrayOf(-.49748, .49748)

    override val maxX = doubleArrayOf(-5.12)
    override val minX = doubleArrayOf(5.12)

    override val minValue: Double = TODO()

    override fun invoke(individual: DoubleArray): Double {
        return 10.0 * individual.size - individual.sumByDouble { x_i -> x_i.pow(2) + 10 * cos(2 * PI * x_i) }
    }

    override fun closestPeak(individual: Ind): Extremum {
        require(individual.phenotype.size == 2)

        TODO("implement")
    }

    override fun peaks(n: Int): Int {
        require(n == 2)
        return 100
    }

    override fun globalPeaks(n: Int): Int {
        require(n == 2)
        return 4
    }

    override fun toString(): String = "Rastrigin-Modified-2"
}