import org.knowm.xchart.QuickChart
import org.knowm.xchart.XChartPanel
import org.knowm.xchart.XYChart
import org.knowm.xchart.XYSeries
import org.knowm.xchart.style.markers.Circle
import java.awt.Color.RED
import java.awt.Color.WHITE
import java.awt.Dimension
import java.awt.EventQueue
import javax.swing.*
import kotlin.random.Random

fun main() {
    val function = FiveUnevenPeakTrap
//    val function = DebFunction1
    val population: Collection<Ind> = List(100) { Ind(function.minX.zip(function.maxX).map{ Random.nextDouble(it.first, it.second) }.toDoubleArray(), function.minX, function.maxX) }

    val functionData = function.getData()
    val chart = QuickChart.getChart("fun", "X", "Y", function.toString(), functionData.first, functionData.second)
    chart.styler.isLegendVisible = false
    chart.styler.chartBackgroundColor = WHITE

    val populationData = population.getData(function)

    val populationSeries = chart.addSeries("population", populationData.first, populationData.second)
    populationSeries.xySeriesRenderStyle = XYSeries.XYSeriesRenderStyle.Scatter
    populationSeries.markerColor = RED
    populationSeries.marker = Circle()

    EventQueue.invokeLater { Tester(
        chart,
        population,
        function,
        populationSeries,
        crossOverProbability = 0.6,
        mutationProbability = 0.001,
        sigmaShare = 0.1,
        alpha = 0.5,
        distanceFunction = EuclideanDistance
    ) }

}

class Tester(
    chart: XYChart,
    private var population: Collection<Ind>,
    function: FitnessFunction,
    series: XYSeries,
    crossOverProbability: Double,
    mutationProbability: Double,
    sigmaShare: Double,
    alpha: Double,
    distanceFunction: (Ind, Ind) -> Double
) : JFrame() {
    private var state: State = State.SELECTION
    private var parentsPool: Collection<Ind> = emptyList()
    private var children: Collection<Ind> = emptyList()

    init {
        contentPane.layout = BoxLayout(contentPane, BoxLayout.Y_AXIS)

        val chartPanel = XChartPanel(chart)
        contentPane.add(chartPanel)

        val toolbar = JPanel()
        toolbar.layout = BoxLayout(toolbar, BoxLayout.X_AXIS)
        contentPane.add(toolbar)

        val iterationBtn = JButton(state.toString())
        iterationBtn.size = Dimension(100, 50)
        toolbar.add(iterationBtn)

        val statsPanel = JPanel()
        statsPanel.layout = BoxLayout(statsPanel, BoxLayout.X_AXIS)
        contentPane.add(statsPanel)

        val iterationBox = JLabel()
        statsPanel.add(iterationBox)

        var iterationsNum = 0
        iterationBtn.addActionListener {
            when (state) {
                State.SELECTION -> {
                    parentsPool = susSelection(population) { it.sharedFitnessFunction(population, function, RunParams(null, 0.0, 0.0, alpha, distanceFunction, sigmaShare, 0.0, 0.0)) }

                    val parentsData = parentsPool.getData(function)
                    series.replaceData(parentsData.first, parentsData.second, null)

                    state = State.CROSSOVER
                }
                State.CROSSOVER -> {
                    val parentPairs = parentsPool.shuffled().chunked(2)
                    val parentChildren = parentPairs.map { crossOver(it[0], it[1], crossOverProbability) }
                    children = parentChildren.flatMap { it.toList() }

                    val data = children.getData(function)
                    series.replaceData(data.first, data.second, null)

                    state = State.MUTATION
                }
                State.MUTATION -> {
                    population = children.map{ mutate(it, mutationProbability) }

                    val data = population.getData(function)
                    series.replaceData(data.first, data.second, null)

                    state = State.SELECTION

                    iterationsNum++
                    iterationBox.text = "$iterationsNum"
                }
            }

            contentPane.revalidate()
            contentPane.repaint()
            iterationBtn.text = state.toString()
        }

        title = "Tester"
        setSize(800, 600)
        defaultCloseOperation = EXIT_ON_CLOSE
        isVisible = true
    }
}

private enum class State { SELECTION, CROSSOVER, MUTATION }

private fun Collection<Ind>.getData(function: FitnessFunction): Pair<DoubleArray, DoubleArray> {
    val xData = map { it.phenotype.first() }
    val yData = map { function(it.phenotype) }

    return Pair(xData.toDoubleArray(), yData.toDoubleArray())
}

fun FitnessFunction.getData(step: Double = 0.01): Pair<DoubleArray, DoubleArray> {
    val xData = ((minX.min()!!/step).toInt()..(maxX.max()!!/step).toInt()).map { it*step }
    val yData = xData.map { invoke(doubleArrayOf(it)) }

    return Pair(xData.toDoubleArray(), yData.toDoubleArray())
}
