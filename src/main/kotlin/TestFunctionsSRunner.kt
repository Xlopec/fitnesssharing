import java.io.File
import java.time.LocalDateTime

fun main() {

    val testParams = listOf(
        RunParams(
            algorithm = ::geneticAlgorithm,
            alpha = 1.0,
            crossOverProbability = 0.6,
            mutationProbability = .001,
            distanceFunction = EuclideanDistance,
            sigmaShare = .2,
            minAfc = 0.0001
        )
    )
    val fitnessFunctions = listOf(FiveUnevenPeakTrap)
    val dimen = 1

    val collector = StatsCollector(File("out/stats/functions_s/dimen_$dimen"), dimen, testParams, fitnessFunctions)

    println("Starting at ${LocalDateTime.now()}. Grab some beer \uD83C\uDF7A and watch \uD83D\uDC40")

    collector.collect()

    println("Done, drink some beer \uD83C\uDF7A and relax")
}