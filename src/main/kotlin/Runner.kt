import java.util.*
import kotlin.math.abs

data class RunParams(
    val algorithm: GeneticAlgorithm?,
    val crossOverProbability: Double,
    val mutationProbability: Double,
    val alpha: Double,
    val distanceFunction: (Ind, Ind) -> Double,
    val sigmaShare: Double,
    val epsilon: Double = .03,
    val sigma: Double = .01,
    val delta: Double = .01,
    val maxNfe: Int = MAX_NFE,
    val minAfc: Double = MIN_AFC,
    val shouldSampleDuration: Boolean = false
) {
    companion object {
        /**
         * Max Number of Fitness function Evaluations
         */
        private const val MAX_NFE = 20_000_000
        /**
         * Min Average Fitness Change
         */
        private const val MIN_AFC = .01
    }
}

sealed class RunStatistics {

    companion object {

        fun ofFailure(nfe: Int): Failure {
            return Failure(nfe)
        }

        fun ofSuccess(
            nfe: Int,
            nSeeds: Collection<Ind>,
            foundGlobalPeaks: Int,
            foundLocalPeaks: Int,
            totalLocalPeaks: Int,
            totalGlobalPeaks: Int,
            k: Double?
        ): Success {

            return Success(
                nfe, nSeeds, foundGlobalPeaks, foundLocalPeaks,
                (foundGlobalPeaks + foundLocalPeaks).toDouble() / (totalLocalPeaks + totalGlobalPeaks),
                foundGlobalPeaks.toDouble() / totalGlobalPeaks,
                if (totalLocalPeaks == 0) null else foundLocalPeaks.toDouble() / totalLocalPeaks,
                k
            )
        }
    }

    data class Failure(override val nfe: Int) : RunStatistics()

    data class Success(override val nfe: Int,
                       val nSeeds: Collection<Ind>,
                       val gPeaks: Int,
                       val lPeaks: Int,
                       val pR: Double,
                       val gPr: Double,
                       val lPr: Double?,
                       val k: Double?
    ): RunStatistics() {

        val fpr: Double = (nSeeds.size - gPeaks - lPeaks).toDouble() / nSeeds.size
    }

    abstract val nfe: Int
}

class Runner(val runParams: RunParams) {

    companion object {
        private const val MAX_TRACK_POPULATIONS = 10
    }

    private class TimeMeasurer {
        private var _timestamp: Long = 0L

        inline val timestamp: Long
            get() = _timestamp

        inline fun <R> update(f: () -> R): R {
            val before = System.currentTimeMillis()
            val result = f()

            _timestamp += System.currentTimeMillis() - before
            return result
        }
    }

    private class CountingGeneticAlgorithm(private val delegate: GeneticAlgorithm) : GeneticAlgorithm {
        private val measurer = TimeMeasurer()

        inline val timestamp: Long
            get() = measurer.timestamp

        override fun invoke(population: Collection<Ind>, fitnessFunction: (DoubleArray) -> Double, params: RunParams): List<Ind> {
            return measurer.update { delegate.invoke(population, fitnessFunction, params) }
        }
    }

    fun run(initialPopulation: Collection<Ind>, fitnessFunction: FitnessFunction, n: Int): RunStatistics {

        tailrec fun iterate(
            iterations: Int,
            popsFitness: Queue<Double>,
            population: Collection<Ind>,
            iterationTimestamp: Long
        ): RunStatistics {

            while (popsFitness.size >= MAX_TRACK_POPULATIONS) {
                popsFitness.poll()
            }

            popsFitness += population.map(fitnessFunction::invoke).average()

            val nfe = iterations * initialPopulation.size

            if (hasNoChangesForLastNEpochs(popsFitness) || nfe > runParams.maxNfe) {

                val k = if (runParams.shouldSampleDuration) {
                    calculateK(iterationTimestamp, iterations, nfe, population, fitnessFunction)
                } else null

                return collectStatistic(population, fitnessFunction, nfe, n, k)
            }

            val wrappedAlg = CountingGeneticAlgorithm(runParams.algorithm!!)
            val nextPopulation = wrappedAlg(population, fitnessFunction, runParams)

            return iterate(iterations + 1, popsFitness, nextPopulation, iterationTimestamp + wrappedAlg.timestamp)
        }

        return iterate(1, LinkedList(), initialPopulation, 0L)
    }

    private fun calculateK(iterationTimestamp: Long,
                           iterations: Int,
                           nfe: Int,
                           population: Collection<Ind>,
                           fitnessFunction: FitnessFunction): Double {

        val measurer = TimeMeasurer()

        measurer.update { for (i in 0..nfe) { population.forEach { fitnessFunction(it) } } }

        return iterationTimestamp / iterations.toDouble() * nfe / measurer.timestamp.toDouble()
    }

    private fun collectStatistic(population: Iterable<Ind>, fitnessFunction: FitnessFunction,
                                 nfe: Int, n: Int, k: Double?): RunStatistics.Success {

        fun stats(foundLocalPeaks: Int, foundGlobalPeaks: Int, seeds: Set<Ind>) = RunStatistics.ofSuccess(
            nfe, seeds, foundGlobalPeaks, foundLocalPeaks,
            fitnessFunction.localPeaks(n), fitnessFunction.globalPeaks(n), k
        )

        tailrec fun collect(lPeaks: Int, gPeaks: Int, seeds: MutableSet<Ind>, it: Iterator<Ind>): RunStatistics.Success {

            if (!it.hasNext()) return stats(lPeaks, gPeaks, seeds)

            val candidate: Ind = it.next()

            require(n == candidate.phenotype.size) {
                "What a Terrible Failure, n=$n,\n" +
                        "candidate=${candidate.phenotype.toList()}"
            }

            if (isPeakCandidate(candidate, seeds)) {
                seeds += candidate

                val extremum: Extremum = fitnessFunction.closestPeak(candidate)

                return when {
                    isFalsePeak(candidate, extremum, fitnessFunction) -> collect(lPeaks, gPeaks, seeds, it)

                    extremum.isGlobal -> collect(lPeaks, gPeaks + 1, seeds, it)

                    else -> collect(lPeaks + 1, gPeaks, seeds, it)
                }
            }

            return collect(lPeaks, gPeaks, seeds, it)
        }

        return collect(0, 0, mutableSetOf(), population.sortedByDescending(fitnessFunction::invoke).iterator())
    }

    private fun hasNoChangesForLastNEpochs(nEpochAverageHealth: Collection<Double>): Boolean {
        return nEpochAverageHealth.size >= MAX_TRACK_POPULATIONS
                && nEpochAverageHealth.zipWithNext { a, b -> abs(a - b) }.all { it < runParams.minAfc }
    }

    private fun isPeakCandidate(candidate: Ind, seeds: Set<Ind>): Boolean {
        return seeds.none { EuclideanDistance(it, candidate) <= runParams.epsilon }
    }

    private fun isFalsePeak(candidate: Ind, extremum: Extremum, fitnessFunction: FitnessFunction): Boolean {
        return EuclideanDistance(extremum.individual, candidate) > runParams.sigma
                || abs(fitnessFunction(extremum.individual) - fitnessFunction(candidate)) > runParams.delta
    }

}

