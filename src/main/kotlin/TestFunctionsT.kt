import kotlin.math.*

private const val Pi5 = 5 * PI
private const val NegLn2 = -1.3862943611198906 // -2 * ln(2.0)

data class Extremum(val individual: Ind, val isGlobal: Boolean)

interface FitnessFunction : (DoubleArray) -> Double {

    fun closestPeak(individual: Ind): Extremum

    fun peaks(n: Int): Int

    fun globalPeaks(n: Int): Int

    fun localPeaks(n: Int): Int = peaks(n) - globalPeaks(n)

    operator fun invoke(ind: Ind) = this(ind.phenotype)

    operator fun invoke(individual: List<Double>) = this(individual.toDoubleArray())

    val minX: DoubleArray
    val maxX: DoubleArray

    val minValue: Double
}

/**
 * 15) Функція рівних максимумів, тестова функція Деба 1 (Equal maxima, Deb’stest function 1):
 */
object DebFunction1 : FitnessFunction {

    private val peakArgs = doubleArrayOf(.1, .3, .5, .7, .9)// all peaks are global

    override val maxX = doubleArrayOf(1.0)
    override val minX = doubleArrayOf(0.0)

    override val minValue: Double = .0

    override fun closestPeak(individual: Ind): Extremum {
        return Extremum(closestPeakIndividual(peakArgs, individual), true)
    }

    override fun peaks(n: Int): Int = globalPeaks(n)

    override fun globalPeaks(n: Int): Int = 5.0.pow(n).toInt()

    override fun invoke(individual: DoubleArray) = individual.sumByDouble(::sin5PiPow6) / individual.size

    override fun toString(): String = "deba1"
}

/**
 * 16) Функція спадаючих максимумів, тестова функція Деба 2 (Decreasing maxima, Deb’s test function 2):
 */
object DebFunction2 : FitnessFunction {

    private const val globalPeak = .1
    private val peakArgs = doubleArrayOf(globalPeak, .3, .5, .7, .9)

    override val maxX = doubleArrayOf(1.0)
    override val minX = doubleArrayOf(0.0)

    override val minValue: Double = .0

    override fun closestPeak(individual: Ind): Extremum {
        return closestPeakIndividual(peakArgs, individual).let { ind ->
            Extremum(ind, ind.phenotype.all { p -> p == globalPeak })
        }
    }

    override fun peaks(n: Int): Int = 5.0.pow(n).toInt()

    override fun globalPeaks(n: Int): Int = 1

    override fun invoke(individual: DoubleArray) = individual.sumByDouble(::f)

    override fun toString(): String = "deba2"

    private fun f(x_i: Double) = E.pow(NegLn2 * ((x_i - .1) / .8).pow(2)) * sin5PiPow6(x_i)
}

/**
 * 18) Функція нерівномірних максимумів, тестова функція Деба 3 (Uneven maxima, Deb’s test function 3)
 */
object DebFunction3 : FitnessFunction {

    private val peakArgs = doubleArrayOf(.08, .247, .451, .681, .934)

    override val maxX = doubleArrayOf(1.0)
    override val minX = doubleArrayOf(0.0)

    override val minValue: Double = .0

    override fun invoke(individual: DoubleArray) = individual.sumByDouble(::f) / individual.size

    override fun closestPeak(individual: Ind): Extremum {
        return Extremum(closestPeakIndividual(peakArgs, individual), true)
    }

    override fun peaks(n: Int): Int = globalPeaks(n)

    override fun globalPeaks(n: Int): Int = 5.0.pow(n).toInt()

    override fun toString(): String = "deba3"

    private fun f(x_i: Double) = sin(Pi5 * (x_i.pow(.75) - .05)).pow(6)
}

/**
 * 19) Функція нерівномірних спадаючих максимумів, тестова функція Деба 4 (Uneven decreasing maxima, Deb’s test function 4):
 */
object DebFunction4 : FitnessFunction {

    private const val globalPeakArg = .08
    private val peakArgs = doubleArrayOf(globalPeakArg, .247, .451, .681, .934)

    override val maxX = doubleArrayOf(1.0)
    override val minX = doubleArrayOf(0.0)

    override val minValue: Double = .0

    override fun closestPeak(individual: Ind): Extremum {
        return closestPeakIndividual(peakArgs, individual).let { peak ->
            Extremum(peak, peak.phenotype.all { p -> p == globalPeakArg })
        }
    }

    override fun peaks(n: Int): Int = 5.0.pow(n).toInt()

    override fun globalPeaks(n: Int): Int = 1

    override fun invoke(individual: DoubleArray) =  individual.sumByDouble(::f)

    override fun toString(): String = "deba4"

    private fun f(x_i: Double) = E.pow(NegLn2 * ((x_i - .08) / .854).pow(2)) * sin(Pi5 * (x_i.pow(.75) - .05)).pow(6)
}

private fun sin5PiPow6(x_i: Double) = sin(Pi5 * x_i).pow(6)

fun closestPeakIndividual(peakArgs: DoubleArray, to: Ind): Ind {
    return Ind(to.phenotype.map { p -> closestPeak(peakArgs, p) }.toDoubleArray(), to.min, to.max)
}

fun closestPeak(peakArgs: DoubleArray, to: Double): Double {
    return peakArgs[peakArgs.withIndex().minBy { (_, x) -> abs(x - to) }!!.index]
}