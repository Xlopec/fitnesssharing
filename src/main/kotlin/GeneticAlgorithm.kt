import kotlin.math.min
import kotlin.random.Random

// seems kotlin doesn't support nested typealiases in generic arguments
typealias SelectionAlgorithm = (population: Collection<Individual<DoubleArray, BitSetChromosome>>, fitnessFunction: (Ind) -> Double) -> Collection<Ind>
typealias GeneticAlgorithm = (population: Collection<Individual<DoubleArray, BitSetChromosome>>, fitnessFunction: (DoubleArray) -> Double, params: RunParams) -> List<Ind>

fun geneticAlgorithm(population: Collection<Ind>, fitnessFunction: (DoubleArray) -> Double, runParams: RunParams): List<Ind> {
    val parentsPool = selection(population, fitnessFunction, ::susSelection, runParams)
    val parentPairs = parentsPool.shuffled().chunked(2)
    val parentChildren = parentPairs.map { crossOver(it[0], it[1], runParams.crossOverProbability) }
    val allChildren = parentChildren.flatMap { it.toList() }

    return allChildren.map { mutate(it, runParams.mutationProbability) }
}

fun mutate(individual: Ind, mutationProbability: Double, random: Random = Random.Default): Ind {
    val chromosome = individual.genotype
    val mutatedChromosome = BitSetChromosome(chromosome.length)

    for (i in 0 until chromosome.length) {
        mutatedChromosome[i] = if (random.nextDouble() > mutationProbability) chromosome[i] else !chromosome[i]
    }

    val res = Ind(mutatedChromosome, individual.min, individual.max)

    check(res.phenotype.size == individual.phenotype.size) {
        "Different phenotype size before: ${individual.phenotype.size} and after mutation: ${res.phenotype.size}"
    }

    return res
}

fun crossOver(individual1: Ind, individual2: Ind, crossOverProbability: Double, random: Random = Random.Default): Pair<Ind, Ind> {
    if (random.nextDouble() > crossOverProbability) {
        return Pair(individual1, individual2)
    }

    val chromosome1 = individual1.genotype
    val chromosome2 = individual2.genotype
    val crossPoint = random.nextInt(chromosome1.length)

    val child1 = BitSetChromosome(chromosome1.length)
    val child2 = BitSetChromosome(chromosome2.length)

    for (i in 0 until crossPoint) {
        child1[i] = chromosome1[i]
        child2[i] = chromosome2[i]
    }

    for (i in crossPoint until chromosome1.length) {
        child1[i] = chromosome2[i]
        child2[i] = chromosome1[i]
    }

    val ind1 = Ind(child1, individual1.min, individual1.max)
    val ind2 = Ind(child2, individual2.min, individual2.max)

    check(ind1.phenotype.size == ind2.phenotype.size)
    check(ind1.phenotype.size == individual1.phenotype.size)

    return Pair(ind1, ind2)
}

fun selection(population: Collection<Ind>, fitnessFunction: (DoubleArray) -> Double,
              selectionAlgorithm: SelectionAlgorithm, params: RunParams): Collection<Ind> {
    return selectionAlgorithm(population) { it.sharedFitnessFunction(population, fitnessFunction, params) }
}

fun susSelection(population: Collection<Ind>, fitnessFunction: (Ind) -> Double): Collection<Ind> {
    return susSelection(population, fitnessFunction, Random.Default)
}

fun susSelection(population: Collection<Ind>, fitnessFunction: (Ind) -> Double, random: Random): Collection<Ind> {
    val indexed = population.toTypedArray()
    val fitness = population.map(fitnessFunction)
    val fitnessMin = min(fitness.min()!!, 0.0)
//    val fitnessMin = fitness.min()!!
    val fitnessFixed = fitness.map { it - fitnessMin }

    val fitnessAccumulated = fitnessFixed.toMutableList()
    for (i in 1 until fitnessAccumulated.size) {
        fitnessAccumulated[i] += fitnessAccumulated[i - 1]
    }

    val totalFitness = fitnessAccumulated.last()
    val childrenNum = population.size
    val pointerLength = totalFitness / childrenNum
    val offset = if (pointerLength == 0.0) 0.0 else random.nextDouble(pointerLength)

    var fitnessIndex = 0
    var selectedIndex = 0
    var pointer = offset
    val selectedIndividuals = IntArray(childrenNum) { -1 }

    while (selectedIndex < selectedIndividuals.size) {
        if (pointer <= fitnessAccumulated[fitnessIndex]) {
            pointer += pointerLength
            selectedIndividuals[selectedIndex++] = fitnessIndex
        } else {
            fitnessIndex++
        }
    }

    return selectedIndividuals.map { indexed[it] }
}