import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized

data class ExpectedPeaksNumber(val n: Int, val localExtrems: Int, val globalExtrems: Int)

data class ExpectedPeakIndividuals(val provided: Ind, val closest: Ind)

data class ExpectedFitnessValue(val provided: Ind, val expectedPoint: Point)

data class TestSetup(
    val testFunction: FitnessFunction,
    val peaksNumber: ExpectedPeaksNumber
) {

    val expectedPeakIndividuals: MutableCollection<ExpectedPeakIndividuals> = mutableListOf()
    val expectedFitnessValues: MutableCollection<ExpectedFitnessValue> = mutableListOf()

    fun provided(i: ExpectedPeakIndividuals) {
        require(i.closest.phenotype.size == i.provided.phenotype.size) { "${i.closest.phenotype.size} != ${i.provided.phenotype.size}" }
        require(i.closest.phenotype.size == peaksNumber.n) { "${i.closest.phenotype.size} != ${peaksNumber.n}" }
        expectedPeakIndividuals += i
    }

    fun provided(i: ExpectedFitnessValue) {
        expectedFitnessValues += i
    }

}

@RunWith(Parameterized::class)
internal class DebFunctionTest(private val setup: TestSetup) {

    companion object {
        private const val ACCURACY = .01

        @JvmStatic
        @Parameterized.Parameters
        fun nDimenResults(): Collection<TestSetup> {

            return setup {
                // Deb 1
                DebFunction1.with(n = 1, localExtrems = 0, globalExtrems = 5) {
                   // provided(doubleArrayOf(.1) isClosestTo doubleArrayOf(.1))
                }
                DebFunction1.with(n = 5, localExtrems = 0, globalExtrems = 3125) {
                   // provided(doubleArrayOf(.1, .3, .5, .7, .9) isClosestTo doubleArrayOf(.1, .3, .5, .7, .9))
                   // provided(doubleArrayOf(.1, .1, .1, .1, .1) isClosestTo doubleArrayOf(.1, .1, .1, .1, .1))
                }

                DebFunction1.with(n = 2, localExtrems = 0, globalExtrems = 25)
                DebFunction1.with(n = 3, localExtrems = 0, globalExtrems = 125)
                // Deb 2
                DebFunction2.with(n = 5, localExtrems = 3124, globalExtrems = 1) {
                  //  provided(doubleArrayOf(.1, .3, .5, .7, .9) isClosestTo doubleArrayOf(.1, .3, .5, .7, .9))
                   // provided(doubleArrayOf(.0, .0, .0, .0, .0) isClosestTo doubleArrayOf(.1, .1, .1, .1, .1))
                   // provided(doubleArrayOf(.9, .9, .9, .9, .9) isClosestTo doubleArrayOf(.9, .9, .9, .9, .9))
                }

                SixHumpCamelBack.with(n = 2, localExtrems = 4, globalExtrems = 2) {
                  //  provided(doubleArrayOf(-.0898, .7126) isClosestTo doubleArrayOf(-.0898, .7126))

                    provided(doubleArrayOf(-.0898, .7126) returns Point(doubleArrayOf(-.0898, .7126),
                        SixHumpCamelBack.minX, SixHumpCamelBack.maxX, 1.031628453))
                    provided(doubleArrayOf(.0898, -.7126) returns Point(doubleArrayOf(-.0898, .7126),
                        SixHumpCamelBack.minX, SixHumpCamelBack.maxX, 1.031628453))

                    provided(doubleArrayOf(-1.7036, .7961) returns Point(doubleArrayOf(-1.7036, .7961),
                        SixHumpCamelBack.minX, SixHumpCamelBack.maxX, .2155))

                    provided(doubleArrayOf(1.7036, -.7961) returns Point(doubleArrayOf(1.7036, -.7961),
                        SixHumpCamelBack.minX, SixHumpCamelBack.maxX, .2155))

                    provided(doubleArrayOf(-1.6071, -.5687) returns Point(doubleArrayOf(1.7036, -.7961),
                        SixHumpCamelBack.minX, SixHumpCamelBack.maxX, -2.1043))

                    provided(doubleArrayOf(1.6071, .5687) returns Point(doubleArrayOf(1.7036, -.7961),
                        SixHumpCamelBack.minX, SixHumpCamelBack.maxX, -2.1043))
                }

                Rastrigin.with(n = 1, localExtrems = 10, globalExtrems = 1) {
                  //  provided(doubleArrayOf(.0) isClosestTo doubleArrayOf(.0))
                    with(doubleArrayOf(.0)) {
                        provided(this returns Point(this, Rastrigin.minX, Rastrigin.maxX, .0))
                    }
                }

                Rastrigin.with(n = 2, localExtrems = 120, globalExtrems = 1) {
                 //   provided(doubleArrayOf(.0, .0) isClosestTo doubleArrayOf(.0, .0))
                    with(doubleArrayOf(.0)) {
                        provided(this returns Point(this, Rastrigin.minX, Rastrigin.maxX, .0))
                    }
                    //provided(doubleArrayOf(.0, .0) returns .0)
                }

                FiveUnevenPeakTrap.with(n = 1, localExtrems = 3, globalExtrems = 2) {
                 //   provided(doubleArrayOf(.0) isClosestTo doubleArrayOf(.0))
                //    provided(doubleArrayOf(1.0) isClosestTo doubleArrayOf(.0))
                    with(doubleArrayOf(.0)) {
                        provided(this returns Point(this, FiveUnevenPeakTrap.minX, FiveUnevenPeakTrap.maxX, 200.0))
                    }
                    with(doubleArrayOf(30.0)) {
                        provided(this returns Point(this, FiveUnevenPeakTrap.minX, FiveUnevenPeakTrap.maxX, 200.0))
                    }
                  //  provided(doubleArrayOf(.0) returns 200.0)
                   // provided(doubleArrayOf(1.0) returns 200.0)
                }
            }
        }
    }

    @Test
    fun `test peaks finding for different n`() {

        val peaks = setup.testFunction.peaks(setup.peaksNumber.n)
        val local = setup.testFunction.localPeaks(setup.peaksNumber.n)
        val global = setup.testFunction.globalPeaks(setup.peaksNumber.n)

        Assert.assertTrue(
            "closest peaks=$peaks, local=$local, global=$global",
            peaks == local + global
        )

        Assert.assertTrue(
            "setup=$setup, local=$local, was=${setup.peaksNumber.localExtrems}",
            local == setup.peaksNumber.localExtrems
        )
        Assert.assertTrue(
            "setup=$setup, global=$global, was=${setup.peaksNumber.globalExtrems}",
            global == setup.peaksNumber.globalExtrems
        )

        setup.expectedPeakIndividuals.forEach { expected ->

            val actualClosest = setup.testFunction.closestPeak(expected.provided).individual

            Assert.assertArrayEquals(
                "setup=$setup, actualClosest=${actualClosest.phenotype.toList()}, " +
                        "closest=${expected.closest.phenotype.toList()}",
                actualClosest.phenotype,
                expected.closest.phenotype,
                ACCURACY
            )
        }
    }

    @Test
    fun `test fitness value calculation`() {
        setup.expectedFitnessValues.forEach { expected ->

            val fitnessValue = setup.testFunction(expected.provided)

            Assert.assertEquals(
                "setup=$setup, actual value=$fitnessValue, " +
                        "expected value=${expected}",
                expected.expectedPoint.fitnessValue,
                fitnessValue,
                ACCURACY
            )
        }
    }

}


private class SetupContext {
    val setups: MutableCollection<TestSetup> = mutableListOf()

    fun FitnessFunction.with(
        n: Int,
        localExtrems: Int,
        globalExtrems: Int,
        block: TestSetup.() -> Unit = {}
    ) {
        setups += TestSetup(this, ExpectedPeaksNumber(n, localExtrems, globalExtrems)).apply(block)
    }

}

//private infix fun DoubleArray.isClosestTo(peak: DoubleArray) = ExpectedPeakIndividuals(Ind(this), Ind(peak))

data class Point(val genotype: DoubleArray, val min: DoubleArray, val max: DoubleArray, val fitnessValue: Double)

private infix fun DoubleArray.returns(point: Point) = ExpectedFitnessValue(Ind(this, point.min, point.max), point)

private fun setup(block: SetupContext.() -> Unit): Collection<TestSetup> {
    return SetupContext().apply(block).setups
}
