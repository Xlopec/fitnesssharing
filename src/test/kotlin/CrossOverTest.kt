import org.junit.Assert
import org.junit.Test
import org.mockito.ArgumentMatchers.anyDouble
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import kotlin.random.Random

class CrossOverTest {

    @Test
    fun testCrossover() {
        val parent1 = "0100001001"
        val parent2 = "1101110011"
        val child1  = "0101110011"
        val child2  = "1100001001"

        val randomMock = mock(Random::class.java)
        `when`(randomMock.nextDouble(anyDouble())).thenReturn(0.0)
        `when`(randomMock.nextInt(anyInt())).thenReturn(3)

        val receivedChild = crossOver(fromStringJustForTest(parent1), fromStringJustForTest(parent2), 1.0, randomMock)

        Assert.assertEquals(child1, receivedChild.first.genotype.toString())
        Assert.assertEquals(child2, receivedChild.second.genotype.toString())
    }

}