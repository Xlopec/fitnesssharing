/*
import org.junit.Assert
import org.junit.Test
import java.util.*

class GrayEncodingTest {

    private val TEST_GENOTYPES = linkedMapOf(
        0.3147  to "0110100110",
        0.1     to "0001010110",
        0.0     to "0000000000",
        0.41510 to "0101010000"
    )

    private val TEST_PHENOTYPES = TEST_GENOTYPES.keys.toList()

    @Test
    fun testEncodingSingleDouble() {
        val phenotype = TEST_PHENOTYPES[0]
        val genotype = TEST_GENOTYPES[phenotype]

        val individual = Ind(doubleArrayOf(phenotype))

        Assert.assertEquals(genotype, individual.genotype.toString())
    }

    @Test
    fun testEncodingMultipleDoubles() {
        val phenotype = TEST_PHENOTYPES
        val genotype = phenotype.map { TEST_GENOTYPES[it] }.joinToString("")

        val individual = Ind(phenotype.toDoubleArray())

        Assert.assertEquals(genotype, individual.genotype.toString())
    }

    @Test
    fun testDecodingSingleDouble() {
        val phenotype = TEST_PHENOTYPES[0]
        val genotype = TEST_GENOTYPES[phenotype]!!

        val individual = fromStringJustForTest(genotype)

        Assert.assertEquals(phenotype, individual.phenotype.first(), 0.001)
    }

    @Test
    fun testDecodingMultipleDoubles() {
        val phenotype = TEST_PHENOTYPES
        val genotype = phenotype.map { TEST_GENOTYPES[it] }

        val bitOrderedGenotype = genotype.joinToString("")
        val bitSet = BitSet(bitOrderedGenotype.length)
        (0 until bitOrderedGenotype.length).filter { bitOrderedGenotype[it] == '1' }.forEach { bitSet.set(it) }
        val individual = Ind(BitSetChromosome(bitSet, phenotype.size * genotype.first()!!.length ))

        Assert.assertArrayEquals(phenotype.toDoubleArray(), individual.phenotype, 0.001)
    }
}*/
