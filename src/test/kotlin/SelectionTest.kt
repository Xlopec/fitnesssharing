/*
import org.junit.Assert.assertArrayEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized
import org.mockito.Mockito.*
import kotlin.random.Random

@RunWith(Parameterized::class)
internal class SelectionTest(private val offset: Double, private val expectedResult: DoubleArray) {

    companion object {
        private val POPULATION = arrayOf(.1, .5, .3, .5, .2, .8).map { Ind(doubleArrayOf(it)) }

        @Parameterized.Parameters(name = "{index}: offset = {0}")
        @JvmStatic
        fun data() = listOf(
            arrayOf(.3, doubleArrayOf(.5, .3, .5, .2, .8, .8)),
            arrayOf(.1, doubleArrayOf(.1, .5, .3, .5, .8, .8)),
            arrayOf(.0, doubleArrayOf(.1, .5, .3, .5, .2, .8)),
            arrayOf(.4, doubleArrayOf(.5, .3, .5, .2, .8, .8))
        )

    }

    @Test
    fun testSimpleSelection() {
        val randomMock = mock(Random::class.java)
        `when`(randomMock.nextDouble(anyDouble())).thenReturn(offset)

        val parentsPool = susSelection(POPULATION, ::linearFitnessFunction, randomMock)

        assertArrayEquals(
            expectedResult,
            parentsPool.map { it.phenotype[0] }.toDoubleArray(),
            1e-10
        )
    }

}

private fun linearFitnessFunction(individual: Ind) = individual.phenotype[0]*/
