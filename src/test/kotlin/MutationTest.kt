import org.junit.Assert
import org.junit.Test
import org.mockito.ArgumentMatchers.anyDouble
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import kotlin.random.Random

class MutationTest {

    @Test
    fun testNoMutation() {
        val testInd  = "0100001001"

        val receivedInd = mutate(fromStringJustForTest(testInd), 0.0)

        Assert.assertEquals(testInd, receivedInd.genotype.toString())
    }

    @Test
    fun testFullMutation() {
        val testInd   = "0100001001"
        val expected  = "1011110110"

        val receivedInd = mutate(fromStringJustForTest(testInd), 1.0)

        Assert.assertEquals(expected, receivedInd.genotype.toString())
    }

    @Test
    fun testPartialMutation() {
        val testInd   = "0100101001"
        val mutated = intArrayOf(0, 2, 8)
        val expected  = "1110101011"

        val randomMock = mock(Random::class.java)
        var i = 0
        `when`(randomMock.nextDouble()).then { if (mutated.contains(i++)) 0.0 else 1.0 }

        val receivedInd = mutate(fromStringJustForTest(testInd), 0.5, randomMock)

        Assert.assertEquals(expected, receivedInd.genotype.toString())
    }

}