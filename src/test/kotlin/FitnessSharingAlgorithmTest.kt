import org.junit.Test
import kotlin.random.Random

class FitnessSharingAlgorithmTest {

    @Test
    fun testRun() {

//        val function = FiveUnevenPeakTrap
//        var population: Collection<Ind> = List(500) { Ind(function.minX.zip(function.maxX).map{ Random.nextDouble(it.first, it.second) }.toDoubleArray(), function.minX, function.maxX) }

        val function = SixHumpCamelBack
        var population: Collection<Ind> = List(10) { Ind(function.minX.zip(function.maxX).map{ Random.nextDouble(it.first, it.second) }.toDoubleArray(), function.minX, function.maxX) }

        val runParams = RunParams(
            algorithm = ::geneticAlgorithm,
            alpha = 1.0,
            crossOverProbability = 0.6,
            mutationProbability = 0.001,
            distanceFunction = EuclideanDistance,
            sigmaShare = 0.2
        )

        fun iterate(pop: Collection<Ind>) = geneticAlgorithm(pop, function , runParams)

        repeat(100) { i ->
            println("$i: ${population.map { function(it) }.average()}")
            population = iterate(population)
        }
    }

}
