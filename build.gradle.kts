import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

buildscript {
    var kotlin_version: String by extra
    kotlin_version = "1.3.11"

    repositories {
        mavenCentral()
    }
    dependencies {
        classpath(kotlin("gradle-plugin", kotlin_version))
    }
}

group = "lab"
version = "1.0-SNAPSHOT"

plugins {
    java
    application
    id("com.github.johnrengelman.shadow") version "4.0.4"
}

apply {
    plugin("kotlin")
}

application {
    mainClassName = "StatsOutputKt"
}

val kotlin_version: String by extra

repositories {
    mavenCentral()
}

tasks.withType(org.jetbrains.kotlin.gradle.tasks.KotlinCompile::class.java).all {
    kotlinOptions {
        // will disable warning about usage of experimental Kotlin features
        freeCompilerArgs += "-Xuse-experimental=kotlin.ExperimentalUnsignedTypes"
        freeCompilerArgs += "-Xuse-experimental=kotlin.Experimental"
    }
}

dependencies {
    compile(kotlin("stdlib-jdk8", kotlin_version))
    compile(kotlin("reflect", kotlin_version))
    compile("org.jetbrains.kotlinx:kotlinx-coroutines-android:1.2.0")
    compile("com.fasterxml.jackson.core", "jackson-databind", "2.9.8")
    compile("org.knowm.xchart", "xchart", "3.5.4")
    compile("com.fasterxml.jackson.module", "jackson-module-kotlin", "2.9.8")
    compile("com.amazonaws", "aws-java-sdk-s3", "1.11.540")
    testCompile("junit", "junit", "4.12")
    testCompile("org.mockito", "mockito-core", "2.27.0")
}

val fatJar = task("fatJar", type = Jar::class) {
    baseName = "${project.name}-fat"
    // manifest Main-Class attribute is optional.
    // (Used only to provide default main class for executable jar)
    manifest {
        attributes["Main-Class"] = "StatsOutputKt"
    }
    from(configurations.runtime.map { if (it.isDirectory) it else zipTree(it) })
    with(tasks["jar"] as CopySpec)
}

tasks {
    "build" {
        dependsOn(fatJar)
    }
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_1_8
}
tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

tasks.withType<JavaExec> {
    jvmArgs = listOf("-Xmx32g")
}
